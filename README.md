# Finalytics

Simple personal finance management and analysis, helping to preserve and increase your savings.

***Project under active development***

# Tech details
App is written in ES6/ES7, based on [Nuxt](https://nuxtjs.org/) framework.

 - Koa 2
 - Vue, Vuex
 - Apollo
 - PostgreSQL

# Requirements

PostgreSQL 9.5
Node.js 9+

# Setup

 1. Clone repo
 2. Install yarn if not already installed [yarn](https://yarnpkg.com)
 3. Run in root folder yarn `yarn && yarn run bootstrap`
 4. Copy config samples from `./packages/finalytics-env/samples` to `./packages/finalytics-env/*.env`

# Run

There is two way to run app: by the docker or on local machine

## Local machine

1. Make sure that config `./packages/finalytics-env/default.env` is exists
2. Setup local PostgresSQL db and create schema from sql file `./packages/finalytics-db/schema.sql`
3. Run in root of project npm run dev
4. Go to localhost:3000

## Docker

1. Make sure that config `./packages/finalytics-env/docker-dev.env` is exists.
2. In root of project folder run `docker-compose up --build`
3. Connect to PostgresSQL database in docker container and create db schema
from sql file `./packages/finalytics-db/schema.sql`
4. Clone [nginx-proxy-companion](https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion) and run in parallel for proxy virtual hosts
5. Add hosts `apollo.finalytics.local` and `finalytics.local` to your /etc/hosts file
6. Go to finalytics.local

# Bug Reports & Feature Requests

Please use the [issue tracker](https://github.com/rvboris/finalytics/issues) to report any bugs or create feature requests.

# License

Finalytics is released as open source software under the CC BY-NC 3.0 license, see the [LICENSE](https://github.com/rvboris/finalytics/blob/master/LICENSE.txt) file in the project root for the full license text.
