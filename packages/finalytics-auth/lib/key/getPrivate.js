const fs = require('fs');
const path = require('path');
const { Promise } = require('bluebird');

const readFile = Promise.promisify(fs.readFile);

let privateKey;

module.exports = async () => {
  const isDev = process.env.NODE_ENV === 'development';
  const isKeyPathSet = !!process.env.JWT_KEY_PRIVATE_PATH;
  const keyPath = isDev && !isKeyPathSet
    ? path.resolve(__dirname, '../../keys/default-jwt-private.pem')
    : process.env.JWT_KEY_PRIVATE_PATH;

  if (!privateKey) {
    privateKey = await readFile(keyPath);
  }

  return privateKey;
};
