module.exports = {
  hashBytes: 32,
  saltBytes: 16,
  iterations: 743243,
};
