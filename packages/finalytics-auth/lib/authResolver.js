module.exports = resolver => async (root, args, ctx) => {
  if (!ctx.state.isAuthenticated) {
    return null;
  }

  return resolver(root, args, ctx);
};
