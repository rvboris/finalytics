const crypto = require('crypto');
const { Promise } = require('bluebird');
const config = require('./config');

const pbkdf2 = Promise.promisify(crypto.pbkdf2);
const randomBytes = Promise.promisify(crypto.randomBytes);

module.exports = async (txt) => {
  const salt = await randomBytes(config.saltBytes);
  const hash = await pbkdf2(
    txt,
    salt,
    config.iterations,
    config.hashBytes,
    'sha512',
  );

  const combined = Buffer.alloc(hash.length + salt.length + 8);

  combined.writeUInt32BE(salt.length, 0, true);
  combined.writeUInt32BE(config.iterations, 4, true);

  salt.copy(combined, 8);
  hash.copy(combined, salt.length + 8);

  return combined;
};
