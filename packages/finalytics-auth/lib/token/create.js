const jwt = require('jsonwebtoken');
const { Promise } = require('bluebird');
const getPrivateKey = require('../key/getPrivate');

const sign = Promise.promisify(jwt.sign);

module.exports = async (userId) => {
  const options = {
    expiresIn: '32m',
    algorithm: 'ES512',
  };

  return sign({ userId }, await getPrivateKey(), options);
};

