module.exports = (header) => {
  if (!header || !header.authorization) {
    return null;
  }

  const parts = header.authorization.split(' ');

  if (parts.length === 2) {
    const scheme = parts[0];
    const credentials = parts[1];

    if (/^Bearer$/i.test(scheme)) {
      return credentials;
    }
  }

  return null;
};
