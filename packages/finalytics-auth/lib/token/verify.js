const jwt = require('jsonwebtoken');
const { Promise } = require('bluebird');
const getPublicKey = require('../key/getPublic');

const verify = Promise.promisify(jwt.verify);

module.exports = async token => verify(token, await getPublicKey(), {
  algorithm: 'ES512',
  maxAge: '32m',
});

