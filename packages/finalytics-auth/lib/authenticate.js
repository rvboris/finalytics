const crypto = require('crypto');
const { Promise } = require('bluebird');

const pbkdf2 = Promise.promisify(crypto.pbkdf2);

module.exports = async (buffer, txt) => {
  const saltBytes = buffer.readUInt32BE(0);
  const hashBytes = buffer.length - saltBytes - 8;
  const iterations = buffer.readUInt32BE(4);
  const salt = buffer.slice(8, saltBytes + 8);
  const hash = buffer.toString('binary', saltBytes + 8);

  const verify = await pbkdf2(txt, salt, iterations, hashBytes, 'sha512');

  return verify.toString('binary') === hash;
};
