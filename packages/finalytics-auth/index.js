require('@finalytics/env')();

const authenticate = require('./lib/authenticate');
const encryptPassword = require('./lib/encryptPassword');
const createToken = require('./lib/token/create');
const verifyToken = require('./lib/token/verify');
const resolveHeader = require('./lib/token/resolveHeader');
const authResolver = require('./lib/authResolver');

module.exports = {
  authenticate,
  encryptPassword,
  createToken,
  verifyToken,
  resolveHeader,
  authResolver,
};
