require('@finalytics/env')();

const db = require('./lib/db');
const queryFile = require('./lib/query-file');

module.exports = {
  db,
  queryFile,
};
