const { Promise } = require('bluebird');
const pgPromise = require('pg-promise');

const initOptions = {
  promiseLib: Promise,
  capSQL: true,
};

const cn = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
};

const pgp = pgPromise(initOptions);

module.exports = pgp(cn);
