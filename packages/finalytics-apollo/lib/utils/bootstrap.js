module.exports = async (db) => {
  const requiredTables = [
    'accounts',
    'categories',
    'currency',
    'entries',
    'profiles',
    'rates',
    'ui_settings',
    'users',
  ];

  const schema = process.env.DB_SCHEMA;
  const checkTables = requiredTables.map(table => db.system.isTableExists({ schema, table }));
  const result = await Promise.all(checkTables);
  const notExistsTables = result.filter(({ exists }) => !exists);

  if (notExistsTables.length === requiredTables.length) {
    throw new Error('The db is not ready, the tables do not exists');
  }

  if (notExistsTables.length > 0) {
    throw new Error(`Some tables not exists: ${notExistsTables.map(({ table }) => table).join(', ')}`);
  }
};
