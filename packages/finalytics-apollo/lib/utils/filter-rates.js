module.exports = (currency, rates) => rates.filter(r => currency.find(c => r.code === c.code));
