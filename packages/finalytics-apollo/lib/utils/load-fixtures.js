const currency = require('../../fixtures/currency.json');
const rates = require('../../fixtures/rates.json');
const filterRates = require('./filter-rates');

module.exports = async (db) => {
  const { count: currencyCount } = await db.currency.count();
  const isCurrencyEmpty = Number(currencyCount) === 0;

  if (isCurrencyEmpty) {
    await db.currency.populate({ data: currency });
  }

  const { count: ratesCount } = await db.rates.count();
  const isRatesEmpty = Number(ratesCount) === 0;

  if (isRatesEmpty) {
    await db.rates.populate({ data: filterRates(currency, rates) });
  }
};
