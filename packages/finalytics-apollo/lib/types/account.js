const currencyType = require('./currency');
const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'Account',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    type: {
      type: GraphQLInt,
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    order: {
      type: GraphQLInt,
    },
    currency: {
      type: currencyType,
      resolve({ currency_id }, args, { db }) {
        return db.currency.findById({ id: currency_id });
      },
    },
    created: {
      type: new GraphQLNonNull(GraphQLString),
    },
    updated: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

