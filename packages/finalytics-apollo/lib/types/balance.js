const accountType = require('./account');
const {
  GraphQLObjectType,
  GraphQLFloat,
  GraphQLNonNull,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'Balance',
  fields: () => ({
    account: {
      type: accountType,
      resolve({ id }, args, { db }) {
        return db.accounts.findById({ id });
      },
    },
    balance: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
  }),
});
