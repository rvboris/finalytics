const accountType = require('./account');
const profileType = require('./profile');
const uiSettingsType = require('./ui-settings');
const rawType = require('./raw');
const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    email: {
      type: new GraphQLNonNull(GraphQLString),
    },
    createdAt: {
      type: new GraphQLNonNull(GraphQLString),
    },
    profile: {
      type: new GraphQLNonNull(profileType),
      resolve({ id }, args, { db }) {
        return db.profiles.findByUserId({ userId: id });
      },
    },
    uiSettings: {
      type: new GraphQLNonNull(uiSettingsType),
      resolve({ id }, args, { db }) {
        return db.uiSettings.findByUserId({ userId: id });
      },
    },
    accounts: {
      type: new GraphQLList(accountType),
      resolve({ id }, args, { db }) {
        return db.accounts.findByUserId({ userId: id });
      },
    },
    categories: {
      type: new GraphQLNonNull(rawType),
      resolve({ id }, args, { db }) {
        return db.categories.findByUserId({ userId: id });
      },
    },
  }),
});
