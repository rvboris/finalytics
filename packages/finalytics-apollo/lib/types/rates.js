const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLFloat,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'Rates',
  fields: () => ({
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    rate: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    created: {
      type: new GraphQLNonNull(GraphQLString),
    },
    updated: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

