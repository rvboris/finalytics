const accountType = require('./account');

const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLFloat,
  GraphQLNonNull,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'Entry',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    description: {
      type: GraphQLString,
    },
    amount: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    currencyRatio: {
      type: new GraphQLNonNull(GraphQLFloat),
      resolve({ currency_ratio }) {
        return currency_ratio;
      },
    },
    fromAccount: {
      type: new GraphQLNonNull(accountType),
      resolve({ credit_id }, args, { db }) {
        return db.accounts.findById({ id: credit_id });
      },
    },
    toAccount: {
      type: new GraphQLNonNull(accountType),
      resolve({ debit_id }, args, { db }) {
        return db.accounts.findById({ id: debit_id });
      },
    },
    updated: {
      type: new GraphQLNonNull(GraphQLString),
    },
    created: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});
