const {
  GraphQLObjectType,
  GraphQLFloat,
  GraphQLNonNull,
} = require('graphql');
const exchange = require('@finalytics/exchange');

module.exports = new GraphQLObjectType({
  name: 'Total',
  fields: () => ({
    amount: {
      type: new GraphQLNonNull(GraphQLFloat),
      async resolve(balances, args, { db, state }, { variableValues }) {
        const calculateNegative = Object.prototype.hasOwnProperty.call(variableValues, 'calculateNegative')
          ? variableValues.calculateNegative
          : true;

        const fx = await exchange();
        const baseCurrency = await db.currency.baseCurrencyByUserId({ userId: state.user.id });

        const convertedBalances =
          balances.map(({ balance, code }) => {
            if (baseCurrency.code !== code) {
              return fx(balance).from(code).to(baseCurrency.code);
            }

            return balance;
          });

        return convertedBalances.reduce((acc, balance) => {
          if (!calculateNegative && balance < 0) {
            return acc;
          }

          return acc + balance;
        }, 0);
      },
    },
  }),
});
