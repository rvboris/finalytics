const { GraphQLScalarType } = require('graphql');

module.exports = new GraphQLScalarType({
  name: 'Raw',
  serialize(value) {
    return value;
  },
});
