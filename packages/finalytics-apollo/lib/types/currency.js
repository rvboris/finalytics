const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'Currency',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    code: {
      type: new GraphQLNonNull(GraphQLString),
    },
    rounding: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    decimalDigits: {
      type: new GraphQLNonNull(GraphQLInt),
      resolve({ decimal_digits }) {
        return decimal_digits;
      },
    },
    symbol: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

