const {
  GraphQLObjectType,
  GraphQLNonNull,
} = require('graphql');
const currencyType = require('./currency');

module.exports = new GraphQLObjectType({
  name: 'Profile',
  fields: () => ({
    baseCurrency: {
      type: new GraphQLNonNull(currencyType),
      resolve({ base_currency_id }, args, { db }) {
        return db.currency.findById({ id: base_currency_id });
      },
    },
  }),
});

