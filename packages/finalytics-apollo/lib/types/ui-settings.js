const {
  GraphQLObjectType,
  GraphQLBoolean,
  GraphQLNonNull,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'UiSettings',
  fields: () => ({
    calculateNegative: {
      type: new GraphQLNonNull(GraphQLBoolean),
      resolve({ ui_calculate_negative }) {
        return ui_calculate_negative;
      },
    },
  }),
});

