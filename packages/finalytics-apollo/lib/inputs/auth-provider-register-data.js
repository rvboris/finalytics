const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } = require('graphql');
const authProviderEmail = require('./auth-provider-email');

module.exports = new GraphQLInputObjectType({
  name: 'AuthProviderRegisterDataInput',
  fields: () => ({
    locale: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: new GraphQLNonNull(authProviderEmail),
    },
  }),
});
