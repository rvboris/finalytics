const {
  GraphQLInputObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLNonNull,
} = require('graphql');

module.exports = new GraphQLInputObjectType({
  name: 'AccountInput',
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    currencyId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});
