const { GraphQLInputObjectType, GraphQLBoolean, GraphQLNonNull } = require('graphql');

module.exports = new GraphQLInputObjectType({
  name: 'UiSettingsInput',
  fields: () => ({
    calculateNegative: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
  }),
});
