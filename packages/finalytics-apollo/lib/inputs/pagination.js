const { GraphQLInputObjectType, GraphQLNonNull, GraphQLInt } = require('graphql');

module.exports = new GraphQLInputObjectType({
  name: 'PaginationInput',
  fields: () => ({
    limit: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    offset: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  }),
});
