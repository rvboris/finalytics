const {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLFloat,
  GraphQLID,
} = require('graphql');

module.exports = new GraphQLInputObjectType({
  name: 'EntryInput',
  fields: () => ({
    description: {
      type: new GraphQLNonNull(GraphQLString),
    },
    amount: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    accountFrom: {
      type: new GraphQLNonNull(GraphQLID),
    },
    accountTo: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});
