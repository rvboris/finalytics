const {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
} = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'RemoveEntriesOutput',
  fields: () => ({
    processed: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  }),
});
