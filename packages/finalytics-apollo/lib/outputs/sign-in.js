const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
} = require('graphql');

const userType = require('../types/user');

module.exports = new GraphQLObjectType({
  name: 'SignInOutput',
  fields: () => ({
    token: {
      type: new GraphQLNonNull(GraphQLString),
    },
    user: {
      type: new GraphQLNonNull(userType),
    },
  }),
});

