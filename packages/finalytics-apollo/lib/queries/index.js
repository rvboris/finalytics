const { GraphQLObjectType } = require('graphql');
const user = require('./user');
const balances = require('./balances');
const entries = require('./entries');
const total = require('./total');
const currency = require('./currency');

module.exports = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    user,
    balances,
    entries,
    total,
    currency,
  }),
});
