const { GraphQLList } = require('graphql');
const entryType = require('../types/entry');
const paginationInput = require('../inputs/pagination');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: new GraphQLList(entryType),
  args: {
    pagination: {
      type: paginationInput,
    },
  },
  resolve: authResolver((root, { pagination }, { db, state }) =>
    db.entries.findByUserId({ userId: state.user.id, pagination })),
};
