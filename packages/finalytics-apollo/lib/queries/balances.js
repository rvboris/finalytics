const { GraphQLList } = require('graphql');
const balanceType = require('../types/balance');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: new GraphQLList(balanceType),
  resolve: authResolver((root, args, { db, state }) =>
    db.balances.findByUserId({ userId: state.user.id })),
};
