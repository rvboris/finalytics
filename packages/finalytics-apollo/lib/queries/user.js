const userType = require('../types/user');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: userType,
  resolve: authResolver((root, args, { db, state }) => db.users.findById({ id: state.user.id })),
};
