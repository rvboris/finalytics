const { GraphQLBoolean } = require('graphql');
const totalType = require('../types/total');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: totalType,
  args: {
    calculateNegative: {
      type: GraphQLBoolean,
    },
  },
  resolve: authResolver((root, args, { db, state }) =>
    db.balances.findByUserId({ userId: state.user.id })),
};
