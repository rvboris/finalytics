const { GraphQLList } = require('graphql');
const currencyType = require('../types/currency');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: new GraphQLList(currencyType),
  resolve: authResolver((root, args, { db }) => db.currency.list()),
};
