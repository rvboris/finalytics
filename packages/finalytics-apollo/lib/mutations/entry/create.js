const { GraphQLNonNull } = require('graphql');
const entryType = require('../../types/entry');
const entryInput = require('../../inputs/entry');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: entryType,
  args: {
    entry: {
      type: new GraphQLNonNull(entryInput),
    },
  },
  resolve: authResolver((root, { entry }, { db, state }) => {
    const currencyRatio = 1;

    return db.entries.createByUserId({ userId: state.user.id, entry: { ...entry, currencyRatio } });
  }),
};
