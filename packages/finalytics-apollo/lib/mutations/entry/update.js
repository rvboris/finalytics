const { GraphQLNonNull, GraphQLID } = require('graphql');
const entryType = require('../../types/entry');
const entryInput = require('../../inputs/entry');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: entryType,
  args: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    entry: {
      type: new GraphQLNonNull(entryInput),
    },
  },
  resolve: authResolver((root, { id, entry }, { db, state }) => {
    const newEntry = Object.assign({}, entry, { currencyRatio: 1 });

    return db.entries.updateById({ userId: state.user.id, id, entry: newEntry });
  }),
};
