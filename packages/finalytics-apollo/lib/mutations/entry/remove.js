const { GraphQLNonNull, GraphQLList, GraphQLID } = require('graphql');
const { authResolver } = require('@finalytics/auth');
const removeEntriesPayload = require('../../outputs/remove-entries');

module.exports = {
  type: new GraphQLNonNull(removeEntriesPayload),
  args: {
    idList: {
      type: new GraphQLNonNull(new GraphQLList(GraphQLID)),
    },
  },
  resolve: authResolver((root, { idList }, { db, state }) =>
    db.entries.removeByIds({ userId: state.user.id, idList })),
};
