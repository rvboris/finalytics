const { GraphQLNonNull, GraphQLID } = require('graphql');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: GraphQLID,
  args: {
    accountId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: authResolver((root, { accountId }, { db, state }) =>
    db.accounts.removeById({ userId: state.user.id, accountId }).then(() => accountId)),
};
