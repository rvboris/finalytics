const { GraphQLNonNull } = require('graphql');
const accountType = require('../../types/account');
const accountInput = require('../../inputs/account');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: accountType,
  args: {
    account: {
      type: new GraphQLNonNull(accountInput),
    },
  },
  resolve: authResolver((root, { account }, { db, state }) => db.accounts.updateById({
    id: account.id,
    userId: state.user.id,
    account: {
      name: account.name,
      currencyId: account.currencyId,
    },
  })),
};
