const { GraphQLNonNull } = require('graphql');
const accountType = require('../../types/account');
const accountInput = require('../../inputs/account');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: accountType,
  args: {
    account: {
      type: new GraphQLNonNull(accountInput),
    },
  },
  resolve: authResolver((root, { account }, { db, state }) => {
    const type = db.accounts.type.personal;
    return db.accounts.createByUserId({ userId: state.user.id, account: { ...account, type } });
  }),
};
