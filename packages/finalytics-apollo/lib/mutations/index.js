const { GraphQLObjectType } = require('graphql');
const registerUser = require('./user/register');
const signInUser = require('./user/sign-in');
const updateToken = require('./user/update-token');
const createEntry = require('./entry/create');
const removeEntries = require('./entry/remove');
const updateEntry = require('./entry/update');
const updateUiSettings = require('./ui-settings/update');
const createAccount = require('./account/create');
const updateAccount = require('./account/update');
const removeAccount = require('./account/remove');

module.exports = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    registerUser,
    signInUser,
    updateToken,
    createEntry,
    removeEntries,
    updateEntry,
    updateUiSettings,
    createAccount,
    updateAccount,
    removeAccount,
  }),
});
