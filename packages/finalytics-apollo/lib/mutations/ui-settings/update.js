const { GraphQLNonNull } = require('graphql');
const uiSettingsType = require('../../types/ui-settings');
const uiSettingsInput = require('../../inputs/ui-settings');
const { authResolver } = require('@finalytics/auth');

module.exports = {
  type: uiSettingsType,
  args: {
    uiSettings: {
      type: new GraphQLNonNull(uiSettingsInput),
    },
  },
  resolve: authResolver((root, { id, uiSettings }, { db, state }) =>
    db.uiSettings.updateByUserId({ userId: state.user.id, id, uiSettings })),
};
