const { GraphQLNonNull } = require('graphql');
const signInPayload = require('../../outputs/sign-in');
const authProviderEmailInput = require('../../inputs/auth-provider-email');
const auth = require('@finalytics/auth');

module.exports = {
  type: signInPayload,
  args: {
    email: {
      type: new GraphQLNonNull(authProviderEmailInput),
    },
  },
  async resolve(root, { email }, { db }) {
    const user = await db.users.findByEmail({ email: email.email });
    const isAuthenticated = await auth.authenticate(user.password, email.password);

    if (isAuthenticated) {
      const token = await auth.createToken(user.id);
      return { token, user };
    }

    return null;
  },
};
