const signInPayload = require('../../outputs/sign-in');
const { authResolver } = require('@finalytics/auth');
const { createToken } = require('@finalytics/auth');

module.exports = {
  type: signInPayload,
  resolve: authResolver(async (root, args, { state }) => {
    if (!state.isAuthenticated) {
      return null;
    }

    return { token: await createToken(state.user.id), user: state.user };
  }),
};
