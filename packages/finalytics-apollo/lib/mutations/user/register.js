const { GraphQLNonNull } = require('graphql');
const signInPayload = require('../../outputs/sign-in');
const authProviderRegisterData = require('../../inputs/auth-provider-register-data');
const auth = require('@finalytics/auth');
const categoriesFixture = require('../../../fixtures/categories');
const accountsFixture = require('../../../fixtures/accounts');
const uiSettingsFixture = require('../../../fixtures/ui-settings');
const localeCurrencyMap = require('../../utils/locale-currency-map');

module.exports = {
  type: signInPayload,
  args: {
    authProvider: {
      type: new GraphQLNonNull(authProviderRegisterData),
    },
  },
  async resolve(root, { authProvider }, { db }) {
    let user;

    if (authProvider.email) {
      user = await db.users.createByEmail({
        email: authProvider.email.email,
        password: await auth.encryptPassword(authProvider.email.password),
      });
    }

    const fallbackLocale = process.env.FALLBACK_LOCALE;

    const categoriesForLocale =
      categoriesFixture[authProvider.locale] || categoriesFixture[fallbackLocale];

    await db.categories.createByUserId({ userId: user.id, data: categoriesForLocale });

    const accountsForLocale =
      accountsFixture[authProvider.locale] || accountsFixture[fallbackLocale];

    const currencyCodeForLocale =
      localeCurrencyMap[authProvider.locale] || localeCurrencyMap[fallbackLocale];

    await Promise.all(accountsForLocale.map(account => new Promise(async (resolve) => {
      const currencyForAccount = await db.currency.findByCode({ code: account.currencyCode });

      await db.accounts.createByUserId({
        userId: user.id,
        account: {
          name: account.name,
          currencyId: currencyForAccount.id,
          type: db.accounts.type.personal,
        },
      });

      resolve();
    })));

    const baseCurrency = await db.currency.findByCode({ code: currencyCodeForLocale });

    await db.profiles.createByUserId({
      userId: user.id,
      profile: {
        baseCurrencyId: baseCurrency.id,
      },
    });

    await db.uiSettings.createByUserId({
      userId: user.id,
      settings: uiSettingsFixture,
    });

    if (user) {
      const token = await auth.createToken(user.id);
      return { token, user };
    }

    return null;
  },
};
