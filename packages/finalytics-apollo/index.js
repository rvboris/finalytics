require('@finalytics/env')();

const Koa = require('koa');
const KoaRouter = require('koa-router');
const koaBody = require('koa-bodyparser');
const { graphqlKoa, graphiqlKoa } = require('apollo-server-koa');
const schema = require('./lib/schema');
const db = require('@finalytics/db');
const auth = require('@finalytics/auth');
const cors = require('koa2-cors');
const loadFixtures = require('./lib/utils/load-fixtures');
const bootstrap = require('./lib/utils/bootstrap');
const logger = require('@finalytics/logger')('apollo');

const isDevelopment = process.env.NODE_ENV === 'development';

const app = new Koa();
const router = new KoaRouter();

app.use(cors({
  origin: '*',
}));

app.use(async (ctx, next) => {
  const token = auth.resolveHeader(ctx.header);

  ctx.state.isAuthenticated = false;
  ctx.state.user = null;

  if (token) {
    try {
      const { userId } = await auth.verifyToken(token);
      ctx.state.user = await db.users.findById({ id: userId });
      ctx.state.isAuthenticated = !!ctx.state.user;
    } catch (e) {
      ctx.state.user = null;
    }
  }

  await next();
});

const graphql = graphqlKoa(ctx => ({
  schema,
  context: {
    state: ctx.state,
    db,
  },
}));

router.post('/graphql', koaBody(), graphql);
router.get('/graphql', graphql);

if (isDevelopment) {
  router.get('/graphiql', graphiqlKoa({ endpointURL: '/graphql' }));
}

app.use(router.routes());
app.use(router.allowedMethods());

process.on('SIGTERM', () => {
  app.close();
});

bootstrap(db)
  .then(() => loadFixtures(db))
  .catch(err => Promise.reject(err))
  .then(() => {
    app.listen(process.env.SERVICE_DATA_PORT);
  })
  .catch((err) => {
    logger.error(err);
    process.exit(1);
  });

