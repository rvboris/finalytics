module.exports = {
  webpack: (config) => {
    const newConfig = { ...config };

    newConfig.entry.main = './src/server/index.js';

    return newConfig;
  },
};
