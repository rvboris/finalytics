require('@finalytics/env')();

const localesIsoMap = require('./src/locales/iso-map');

const LOCALES = process.env.LOCALES.split(',');

module.exports = {
  srcDir: 'src/',

  env: {
    SERVICE_DATA_PUBLIC_URL: process.env.SERVICE_DATA_PUBLIC_URL,
    SERVICE_DATA_LOCAL_URL: process.env.SERVICE_DATA_LOCAL_URL,
    LOCALES,
    FALLBACK_LOCALE: process.env.FALLBACK_LOCALE,
    CLIENT_HOST: process.env.CLIENT_HOST,
    CLIENT_PORT: process.env.CLIENT_PORT,
  },

  plugins: [
    // '~/plugins/i18n',
    '~/plugins/element-ui',
    '~/plugins/virtual-scroll',
    '~/plugins/currency-filter',
    { src: '~/plugins/update-token', ssr: false },
  ],

  build: {
    cache: true,
    parallel: false,

    extend(config) {
      config.module.rules.push({
        test: /\.postcss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          { loader: 'postcss-loader' },
        ],
      });
    },
  },

  babel: {
    presets: [['vue-app', { modules: false }]],
    plugins: [['component', [
      {
        libraryName: 'element-ui',
        styleLibraryName: 'theme-chalk',
      },
    ]]],
    comments: false,
  },

  // Add apollo module
  modules: [
    ['nuxt-i18n', {
      parsePages: false,
      locales: LOCALES.map(locale => ({
        code: locale,
        file: `${locale}.json`,
        iso: localesIsoMap[locale],
      })),
      defaultLocale: process.env.FALLBACK_LOCALE,
      lazy: true,
      langDir: 'locales/',
    }],
    '@nuxtjs/apollo',
  ],

  // Give apollo module options
  apollo: {
    clientConfigs: {
      default: '~/apollo/client-configs/default.js',
    },
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'starter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },
  /*
  ** Global CSS
  */
  css: [
    '~assets/css/reset.css',
    'element-ui/lib/theme-chalk/index.css',
    '~assets/css/typo.css',
    '~assets/css/main.css',
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
};
