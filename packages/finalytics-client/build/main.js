require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("@finalytics/env");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)();

const log = __webpack_require__(2);
const Koa = __webpack_require__(3);
const { Nuxt, Builder } = __webpack_require__(4);
const config = __webpack_require__(5);

const logger = log('client');

async function start() {
  const app = new Koa();
  const host = process.env.CLIENT_HOST;
  const port = process.env.CLIENT_PORT;

  config.dev = !(app.env === 'production');

  const nuxt = new Nuxt(config);

  if (config.dev) {
    new Builder(nuxt).build().catch(e => {
      logger.error(e);
      process.exit(1);
    });
  }

  app.use(async (ctx, next) => {
    await next();

    ctx.status = 200;

    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve);
      ctx.res.on('finish', resolve);

      nuxt.render(ctx.req, ctx.res, promise => {
        promise.then(resolve).catch(reject);
      });
    });
  });

  logger.info(`Server listening on ${host}:${port}`);

  app.listen(port, host);
}

start();

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("@finalytics/logger");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("koa");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("nuxt-edge");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0)();

const localesIsoMap = __webpack_require__(6);

const LOCALES = process.env.LOCALES.split(',');

module.exports = {
  srcDir: 'src/',

  env: {
    SERVICE_DATA_PUBLIC_URL: process.env.SERVICE_DATA_PUBLIC_URL,
    SERVICE_DATA_LOCAL_URL: process.env.SERVICE_DATA_LOCAL_URL,
    LOCALES,
    FALLBACK_LOCALE: process.env.FALLBACK_LOCALE,
    CLIENT_HOST: process.env.CLIENT_HOST,
    CLIENT_PORT: process.env.CLIENT_PORT
  },

  plugins: [
  // '~/plugins/i18n',
  '~/plugins/element-ui', '~/plugins/virtual-scroll', '~/plugins/currency-filter', { src: '~/plugins/update-token', ssr: false }],

  build: {
    cache: true,
    parallel: false,

    extend(config) {
      config.module.rules.push({
        test: /\.postcss$/,
        use: ['vue-style-loader', 'css-loader', { loader: 'postcss-loader' }]
      });
    }
  },

  babel: {
    presets: [['vue-app', { modules: false }]],
    plugins: [['component', [{
      libraryName: 'element-ui',
      styleLibraryName: 'theme-chalk'
    }]]],
    comments: false
  },

  // Add apollo module
  modules: [['nuxt-i18n', {
    parsePages: false,
    locales: LOCALES.map(locale => ({
      code: locale,
      file: `${locale}.json`,
      iso: localesIsoMap[locale]
    })),
    defaultLocale: process.env.FALLBACK_LOCALE,
    lazy: true,
    langDir: 'locales/'
  }], '@nuxtjs/apollo'],

  // Give apollo module options
  apollo: {
    clientConfigs: {
      default: '~/apollo/client-configs/default.js'
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'starter',
    meta: [{ charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' }, { hid: 'description', name: 'description', content: 'Nuxt.js project' }],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
  ** Global CSS
  */
  css: ['~assets/css/reset.css', 'element-ui/lib/theme-chalk/index.css', '~assets/css/typo.css', '~assets/css/main.css'],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' }
};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = {
  ru: 'ru-RU',
  en: 'en-US'
};

/***/ })
/******/ ]);
//# sourceMappingURL=main.map