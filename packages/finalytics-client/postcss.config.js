module.exports = {
  plugins: {
    'postcss-nested': true,
    'postcss-cssnext': {
      browsers: ['last 2 versions'],
    },
    cssnano: {
      preset: 'default',
    },
    'postcss-pxtorem': {
      propList: ['*'],
    },
  },
};
