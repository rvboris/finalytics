import Cookies from 'universal-cookie';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

export default ({ env, req }) => {
  const httpLink = new HttpLink({
    uri: process.server ? env.SERVICE_DATA_LOCAL_URL : env.SERVICE_DATA_PUBLIC_URL,
  });

  const getToken = () => {
    const cookies = new Cookies(process.server ? req.headers.cookie : undefined);
    const auth = cookies.get('auth');

    return auth || '';
  };

  const middlewareLink = new ApolloLink((operation, forward) => {
    const token = getToken();

    operation.setContext({
      headers: { authorization: `Bearer ${token}` },
    });

    return forward(operation);
  });

  const link = middlewareLink.concat(httpLink);

  const defaultOptions = {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    mutate: {
      errorPolicy: 'all',
    },
  };

  return {
    link,
    cache: new InMemoryCache({
      addTypename: false,
    }),
    defaultOptions,
  };
};
