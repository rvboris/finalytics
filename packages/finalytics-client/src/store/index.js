import Vue from 'vue';
import get from 'lodash/get';
import Cookies from 'universal-cookie';
import signInMutation from '../apollo/mutations/sign-in.gql';
import registerMutation from '../apollo/mutations/register.gql';
import updateTokenMutation from '../apollo/mutations/update-token.gql';
import updateUiSettingsMutation from '../apollo/mutations/update-ui-settings.gql';
import userQuery from '../apollo/queries/user.gql';
import currencyQuery from '../apollo/queries/currency.gql';

export const state = () => ({
  tokenUpdateInterval: null,
  auth: null,
  user: null,
  currencyList: [],
});

export const mutations = {
  SET_AUTH(state, token) {
    state.auth = token;
  },
  SET_USER(state, user) {
    state.user = user;
  },
  SET_TOKEN_INTERVAL(state, interval) {
    state.tokenUpdateInterval = interval;
  },
  SET_UI_SETTINGS(state, uiSettings) {
    state.user = Object.assign({}, state.user, { uiSettings });
  },
  SET_CURRENCY_LIST(state, currencyList) {
    Vue.set(state, 'currencyList', currencyList);
  },
};

export const actions = {
  async nuxtServerInit({ commit, dispatch }) {
    const { defaultClient } = this.app.apolloProvider;
    const { data } = await defaultClient.mutate({ mutation: updateTokenMutation });
    const auth = get(data, 'updateToken.token', null);

    commit('SET_AUTH', auth);

    if (auth) {
      await dispatch('getUser');
    }
  },

  async getUser({ commit, getters, dispatch }) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.query({ query: userQuery });

    commit('SET_USER', data.user);

    const balanceParams = {
      calculateNegative: getters.uiSettings('calculateNegative'),
    };

    await Promise.all([
      dispatch('balances/update', balanceParams, { root: true }),
      dispatch('getCurrencyList'),
    ]);
  },

  updateAuth({ commit }, auth) {
    const cookies = new Cookies();

    commit('SET_AUTH', auth);

    if (auth) {
      cookies.set('auth', auth);
    } else {
      cookies.remove('auth');
    }
  },

  async signIn({ state, dispatch }, payload) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.mutate({ mutation: signInMutation, variables: payload });
    const auth = get(data, 'signInUser.token', null);

    dispatch('updateAuth', auth);

    if (state.auth) {
      await dispatch('getUser');
    }
  },

  async register({ state, dispatch }, payload) {
    const { locale } = window.$nuxt.$i18n;
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.mutate({
      mutation: registerMutation,
      variables: { ...payload, locale },
    });

    const auth = get(data, 'registerUser.token', null);

    dispatch('updateAuth', auth);

    if (state.auth) {
      await dispatch('getUser');
    }
  },

  tokenUpdateInterval({ commit, state, dispatch }) {
    if (process.server || state.tokenUpdateInterval) {
      return;
    }

    const client = this.app.apolloProvider.defaultClient;
    const updateInterval = 16 * 60 * 1000;

    // eslint-disable-next-line
    const interval = window.setInterval(() => {
      if (!state.auth) {
        return;
      }

      client.mutate({ mutation: updateTokenMutation }).then(({ data }) => {
        const auth = get(data, 'updateToken.token', null);
        dispatch('updateAuth', auth);
      });
    }, updateInterval);

    commit('SET_TOKEN_INTERVAL', interval);
  },

  signOut({ dispatch }) {
    dispatch('updateAuth', null);
  },

  async updateUiSettings({ commit }, payload) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.mutate({
      mutation: updateUiSettingsMutation,
      variables: payload,
    });

    const uiSettings = get(data, 'updateUiSettings', {});

    commit('SET_UI_SETTINGS', uiSettings);
  },

  async getCurrencyList({ commit }) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.query({ query: currencyQuery });

    const currencyList = get(data, 'currency', {});

    commit('SET_CURRENCY_LIST', currencyList);
  },
};

export const getters = {
  uiSettings(state) {
    return key => state.user.uiSettings[key];
  },
  currencyList(state) {
    return state.currencyList.map(currency =>
      Object.assign({}, currency, { name: window.$nuxt.$i18n.t(`currency.${currency.code}`) }));
  },
};
