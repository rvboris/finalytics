import Vue from 'vue';
import balancesQuery from '../apollo/queries/balances.gql';
import totalQuery from '../apollo/queries/total.gql';
import createAccountMutation from '../apollo/mutations/create-account.gql';
import updateAccountMutation from '../apollo/mutations/update-account.gql';
import removeAccountMutation from '../apollo/mutations/remove-account.gql';

export const state = () => ({
  balances: [],
  total: 0,
});

export const mutations = {
  SET_BALANCES(state, balances) {
    Vue.set(state, 'balances', balances);
  },
  SET_TOTAL(state, total) {
    state.total = total;
  },
};

export const actions = {
  async update({ commit }, payload = {}) {
    const client = this.app.apolloProvider.defaultClient;
    const { data: dataBalances } = await client.query({ query: balancesQuery });
    const balances = dataBalances.balances || [];

    commit('SET_BALANCES', balances);

    const { data: dataTotal } = await client.query({ query: totalQuery, variables: payload });

    commit('SET_TOTAL', dataTotal.total.amount);
  },

  async createAccount({ dispatch, rootState }, payload) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.mutate({
      mutation: createAccountMutation,
      variables: payload,
    });

    const { id } = data.createAccount;

    if (id) {
      await dispatch('update', { calculateNegative: rootState.user.uiSettings.calculateNegative });
    }

    return id;
  },

  async updateAccount({ dispatch, rootState }, payload) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.mutate({
      mutation: updateAccountMutation,
      variables: payload,
    });

    const { id } = data.updateAccount;

    if (id) {
      await dispatch('update', { calculateNegative: rootState.user.uiSettings.calculateNegative });
    }

    return id;
  },

  async removeAccount({ dispatch, rootState }, payload) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.mutate({
      mutation: removeAccountMutation,
      variables: payload,
    });

    const accountId = data.removeAccount;

    if (accountId) {
      await dispatch('update', { calculateNegative: rootState.user.uiSettings.calculateNegative });
    }

    return accountId;
  },
};

export const getters = {
  accounts(state) {
    return state.balances.map(balance => balance.account);
  },
};
