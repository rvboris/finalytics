import Vue from 'vue';
import entriesQuery from '../apollo/queries/entries.gql';

export const state = () => ({
  entries: [],
});

export const mutations = {
  SET_ENTRIES(state, entries) {
    Vue.set(state, 'entries', entries);
  },
  ADD_ENTRIES(state, entries) {
    state.entries.push(...entries);
  },
};

export const actions = {
  async load({ commit }) {
    const client = this.app.apolloProvider.defaultClient;
    const { data } = await client.query({ query: entriesQuery });
    const entries = data.entries || [];

    commit('SET_ENTRIES', entries);
  },
};
