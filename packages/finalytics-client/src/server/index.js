require('@finalytics/env')();

const log = require('@finalytics/logger');
const Koa = require('koa');
const { Nuxt, Builder } = require('nuxt-edge');
const config = require('../../nuxt.config');

const logger = log('client');

async function start() {
  const app = new Koa();
  const host = process.env.CLIENT_HOST;
  const port = process.env.CLIENT_PORT;

  config.dev = !(app.env === 'production');

  const nuxt = new Nuxt(config);

  if (config.dev) {
    new Builder(nuxt).build().catch((e) => {
      logger.error(e);
      process.exit(1);
    });
  }

  app.use(async (ctx, next) => {
    await next();

    ctx.status = 200;

    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve);
      ctx.res.on('finish', resolve);

      nuxt.render(ctx.req, ctx.res, (promise) => {
        promise.then(resolve).catch(reject);
      });
    });
  });

  logger.info(`Server listening on ${host}:${port}`);

  app.listen(port, host);
}

start();
