const update = require('./update');
const fx = require('money');
const querystring = require('querystring');
const db = require('@finalytics/db');
const logger = require('@finalytics/logger')('f/exchange');

let interval;

const getUpdateUrl = () => {
  const params = querystring.stringify({
    app_id: process.env.OPEN_EXCHANGE_RATES_APP_ID,
  });

  return `${process.env.OPEN_EXCHANGE_RATES_URL}?${params}`;
};

module.exports = async () => {
  const updateInterval = 60 * 60 * 1000; // 1h
  const updateUrl = getUpdateUrl();
  const baseCurrencyCode = 'USD';

  fx.base = baseCurrencyCode;

  if (interval) {
    return fx;
  }

  interval = setInterval(() => {
    update(updateUrl).then(({ data }) => {
      fx.rates = data.rates;

      const rates = Object.entries(data.rates).map(([code, rate]) => ({
        code,
        rate,
      }));

      return db.rates.update({ data: rates });
    }).then(() => {
      logger.info('rates updated');
    });
  }, updateInterval);

  const rates = await db.rates.list();

  fx.rates = rates.reduce((acc, { rate, code }) => {
    acc[code] = Number(rate);
    return acc;
  }, {});

  return fx;
};
