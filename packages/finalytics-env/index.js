const dotenv = require('dotenv-safe');
const path = require('path');
const fs = require('fs');

let envLoaded;

module.exports = () => {
  if (envLoaded) {
    return;
  }

  const envPath = process.env.ENV_FILE_PATH ? path.resolve(process.env.ENV_FILE_PATH) : '';
  const useEnvFile = fs.existsSync(envPath);

  dotenv.config({
    allowEmptyValues: true,
    sample: useEnvFile ? undefined : path.resolve(__dirname, './samples/default.env.sample'),
    path: useEnvFile ? envPath : path.resolve(__dirname, './default.env'),
  });

  // eslint-disable-next-line
  const logger = require('@finalytics/logger')('env');

  logger.info(useEnvFile ? `use ${envPath} env file` : 'use default env file');

  envLoaded = true;
};
