module.exports = ({ db }, { userId, data = {} }) =>
  db.one('UPDATE categories SET data = $2 WHERE user_id = $1 RETURNING data', [
    userId,
    data,
  ]);
