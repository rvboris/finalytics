module.exports = ({ db, name }, { userId }) => db.one({
  name,
  text: 'SELECT data FROM categories WHERE user_id = $1',
  values: userId,
}).then(result => result.data);

