module.exports = ({ db }, { userId, data = {} }) =>
  db.one('INSERT INTO categories(user_id, data) VALUES($1, $2) RETURNING data', [
    userId,
    data,
  ]);
