module.exports = ({ db }, { userId, type = 1 }) => db
  .any(`SELECT
        account_balances.id,
        account_balances.balance,
        rates.rate,
        rates.code,
        currency.decimal_digits
        FROM account_balances
        INNER JOIN accounts ON account_balances.id = accounts.id
        INNER JOIN users ON accounts.user_id = users.id
        INNER JOIN currency ON accounts.currency_id = currency.id
        INNER JOIN rates ON rates.code = currency.code
        WHERE
        users.id = $1
        AND
        accounts.type = $2`, [userId, type])
  .then(balances =>
    balances.map(balance =>
      ({ ...balance, balance: Number(balance.balance), rate: Number(balance.rate) })));
