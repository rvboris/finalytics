module.exports = ({ db }, { userId, profile = {} }) =>
  db.one('INSERT INTO profiles(user_id, base_currency_id) VALUES($1, $2) RETURNING *', [
    userId,
    profile.baseCurrencyId,
  ]);
