module.exports = ({ db }, { userId }) => db.one('SELECT * FROM profiles WHERE user_id = $1', [userId]);
