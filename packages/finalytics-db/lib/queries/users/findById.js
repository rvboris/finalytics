module.exports = ({ db, name }, { id }) => db.one({
  name,
  text: 'SELECT * FROM users WHERE users.id = $1',
  values: [id],
});
