module.exports = ({ db, name }, { email }) => db.one({
  name,
  text: 'SELECT * FROM users WHERE users.email = $1',
  values: [email],
});
