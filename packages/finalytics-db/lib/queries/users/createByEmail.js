module.exports = ({ db }, { email, password }) =>
  db.one('INSERT INTO users(email, password) VALUES($1, $2) RETURNING id, email, created_at', [email, password]);
