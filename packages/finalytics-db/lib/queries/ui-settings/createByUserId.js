module.exports = ({ db }, { userId, settings = {} }) =>
  db.one('INSERT INTO ui_settings(user_id, ui_calculate_negative) VALUES($1, $2) RETURNING *', [
    userId,
    settings.calculateNegative,
  ]);
