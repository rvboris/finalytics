module.exports = ({ db }, { userId, uiSettings = {} }) =>
  db.one('UPDATE ui_settings SET ui_calculate_negative=$2 WHERE ui_settings.user_id=$1 RETURNING *', [
    userId,
    uiSettings.calculateNegative,
  ]);
