module.exports = ({ db }, { userId }) => db.one('SELECT * FROM ui_settings WHERE user_id = $1', [userId]);
