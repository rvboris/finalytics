
module.exports = ({ db, name }, { userId }) => db.one({
  name,
  text: `SELECT
    currency.id,
    rates.code,
    rates.rate,
    currency.rounding,
    currency.decimal_digits,
    currency.symbol
    FROM
    users
    INNER JOIN profiles ON profiles.user_id = users.id
    INNER JOIN currency ON profiles.base_currency_id = currency.id
    INNER JOIN rates ON rates.code = currency.code
    WHERE
    users.id = $1`,
  values: userId,
}).then(baseCurrency => ({ ...baseCurrency, rate: Number(baseCurrency.rate) }));
