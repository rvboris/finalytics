module.exports = ({ db }, { data = [] }) =>
  db.tx((t) => {
    const queries = data.map(currency =>
      // eslint-disable-next-line
      t.none('INSERT INTO currency(symbol, decimal_digits, rounding, code) VALUES(${symbol}, ${decimalDigits}, ${rounding}, ${code})', currency));

    return t.batch(queries);
  });
