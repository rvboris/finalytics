module.exports = ({ db, name }, { id }) => db.one({
  name,
  text: 'SELECT * FROM currency WHERE currency.id = $1',
  values: id,
});

