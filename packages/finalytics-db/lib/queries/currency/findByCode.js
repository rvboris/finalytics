module.exports = ({ db, name }, { code }) => db.one({
  name,
  text: 'SELECT * FROM currency WHERE currency.code = $1',
  values: code,
});

