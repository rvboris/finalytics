module.exports = ({ db }, { userId, limit = 50, offset = 0 }) =>
  db
    .any('SELECT * FROM entries WHERE user_id = $1 LIMIT $2 OFFSET $3', [userId, limit, offset])
    .then(entries => entries.map(entry => ({ ...entry, amount: Number(entry.amount) })));
