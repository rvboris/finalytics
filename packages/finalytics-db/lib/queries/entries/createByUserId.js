module.exports = ({ db }, { userId, entry = {} }) =>
  db.one(`INSERT INTO entries(user_id, description, amount, credit_id, debit_id, currency_ratio)
  VALUES($1, $2, $3, $4, $5, $6) RETURNING *`, [
    userId,
    entry.description,
    entry.amount,
    entry.accountFrom,
    entry.accountTo,
    entry.currencyRatio,
  ]).then(updatedEntry => ({ ...updatedEntry, amount: Number(updatedEntry.amount) }));
