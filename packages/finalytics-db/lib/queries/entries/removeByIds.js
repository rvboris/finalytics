module.exports = ({ db }, { userId, idList = [] }) =>
  db
    .result(`DELETE FROM entries WHERE id IN (${idList.join(', ')}) AND user_id = $1`, userId)
    .then(result => ({ processed: result.rowCount }));
