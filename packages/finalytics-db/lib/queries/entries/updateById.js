module.exports = ({ db }, { id, userId, entry = {} }) => db.one('UPDATE entries SET description = $1, amount = $2, credit_id = $3, debit_id = $4, currency_ratio = $5 WHERE id = $6 AND user_id = $7 RETURNING *', [
  entry.description,
  entry.amount,
  entry.accountFrom,
  entry.accountTo,
  entry.currencyRatio,
  id,
  userId,
]).then(updatedEntry => ({ ...updatedEntry, amount: Number(updatedEntry.amount) }));
