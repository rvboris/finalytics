module.exports = ({ db, name }, { schema, table }) => db.one({
  name,
  text: 'SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = $1 AND table_name = $2)',
  values: [
    schema,
    table,
  ],
}).then(result => ({ table, ...result }));

