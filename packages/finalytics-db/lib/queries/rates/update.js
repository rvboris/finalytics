module.exports = ({ db }, { data = [] }) =>
  db.tx((t) => {
    const queries = data.map(({ rate, code }) => t.none('UPDATE rates SET rate=$1 WHERE rates.code=$2', [rate, code]));
    return t.batch(queries);
  });
