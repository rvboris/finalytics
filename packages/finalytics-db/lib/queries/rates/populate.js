module.exports = ({ db }, { data = [] }) =>
  db.tx((t) => {
    const queries = data.map(rate =>
      // eslint-disable-next-line
      t.none('INSERT INTO rates(code, rate) VALUES(${code}, ${rate})', rate));

    return t.batch(queries);
  });
