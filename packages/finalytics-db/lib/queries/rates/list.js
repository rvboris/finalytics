module.exports = ({ db, name }) => db.any({
  name,
  text: 'SELECT * FROM rates',
}).then(rates => rates.map(rate => ({ ...rate, rate: Number(rate.rate) })));
