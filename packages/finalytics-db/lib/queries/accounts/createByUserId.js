module.exports = ({ db }, { userId, account = {} }) =>
  db.one('INSERT INTO accounts(name, user_id, currency_id, type) VALUES($2, $1, $3, $4) RETURNING *', [
    userId,
    account.name,
    account.currencyId,
    account.type,
  ]);
