module.exports = ({ db }, { id, userId, account = {} }) =>
  db.one('UPDATE accounts SET name = $3, currency_id=$4 WHERE id = $1 AND user_id = $2 RETURNING *', [
    id,
    userId,
    account.name,
    account.currencyId,
  ]);
