module.exports = ({ db }, { userId }) => db.any('SELECT * FROM accounts WHERE accounts.user_id = $1', [userId]);
