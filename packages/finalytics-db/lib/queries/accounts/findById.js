module.exports = ({ db, name }, { id }) => db.one({
  name,
  text: 'SELECT * FROM accounts WHERE accounts.id = $1',
  values: id,
});

