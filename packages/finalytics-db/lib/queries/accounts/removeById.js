module.exports = ({ db }, { userId, accountId }) =>
  db
    .result('DELETE FROM accounts WHERE id = $1 AND user_id = $2', [
      accountId,
      userId,
    ])
    .then(() => ({ accountId }));
