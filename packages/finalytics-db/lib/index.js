const queries = require('require-glob').sync('./queries/**/*.js');
const { db, queryFile } = require('@finalytics/pg');
const { flatten, unflatten } = require('./utils/transformer');

const flatQueries = flatten(queries);

const wrappedQueries = Object.keys(flatQueries).reduce((acc, name) => {
  acc[name] = typeof flatQueries[name] === 'function'
    ? (...args) => flatQueries[name]({ db, queryFile, name }, ...args)
    : flatQueries[name];

  return acc;
}, {});

const deepQueries = unflatten(wrappedQueries);

module.exports = deepQueries;

