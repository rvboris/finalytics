--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgraphql_watch; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA postgraphql_watch;


ALTER SCHEMA postgraphql_watch OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = postgraphql_watch, pg_catalog;

--
-- Name: notify_watchers_ddl(); Type: FUNCTION; Schema: postgraphql_watch; Owner: postgres
--

CREATE FUNCTION notify_watchers_ddl() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
begin
  perform pg_notify(
    'postgraphql_watch',
    json_build_object(
      'type',
      'ddl',
      'payload',
      (select json_agg(json_build_object('schema', schema_name, 'command', command_tag)) from pg_event_trigger_ddl_commands() as x)
    )::text
  );
end;
$$;


ALTER FUNCTION postgraphql_watch.notify_watchers_ddl() OWNER TO postgres;

--
-- Name: notify_watchers_drop(); Type: FUNCTION; Schema: postgraphql_watch; Owner: postgres
--

CREATE FUNCTION notify_watchers_drop() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
begin
  perform pg_notify(
    'postgraphql_watch',
    json_build_object(
      'type',
      'drop',
      'payload',
      (select json_agg(distinct x.schema_name) from pg_event_trigger_dropped_objects() as x)
    )::text
  );
end;
$$;


ALTER FUNCTION postgraphql_watch.notify_watchers_drop() OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: entries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE entries (
    id integer NOT NULL,
    user_id integer NOT NULL,
    description character varying(1024),
    amount numeric(20,6) NOT NULL,
    credit_id integer NOT NULL,
    debit_id integer NOT NULL,
    currency_ratio numeric(15,6) DEFAULT 1 NOT NULL,
    created_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    updated_at timestamp(6) with time zone DEFAULT now() NOT NULL
);


ALTER TABLE entries OWNER TO postgres;

--
-- Name: account_ledgers; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW account_ledgers AS
 SELECT entries.credit_id AS account_id,
    entries.id AS entry_id,
    (entries.amount * entries.currency_ratio) AS amount,
    entries.created_at AS entry_created,
    entries.updated_at AS entry_updated
   FROM entries
UNION ALL
 SELECT entries.debit_id AS account_id,
    entries.id AS entry_id,
    (0.0 - (entries.amount * entries.currency_ratio)) AS amount,
    entries.created_at AS entry_created,
    entries.updated_at AS entry_updated
   FROM entries;


ALTER TABLE account_ledgers OWNER TO postgres;

--
-- Name: ledgers_on_date(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION ledgers_on_date(created timestamp with time zone) RETURNS SETOF account_ledgers
    LANGUAGE sql
    AS $$

  SELECT * FROM account_ledgers WHERE account_ledgers.entry_created <= created

$$;


ALTER FUNCTION public.ledgers_on_date(created timestamp with time zone) OWNER TO postgres;

--
-- Name: update_balances(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_balances() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN

	REFRESH MATERIALIZED VIEW account_balances;

	RETURN NULL;

END

$$;


ALTER FUNCTION public.update_balances() OWNER TO postgres;

--
-- Name: update_modified_column(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_modified_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN

IF

	ROW ( NEW.* ) IS DISTINCT 

FROM

	ROW ( OLD.* ) THEN

	NEW.updated_at = now();

RETURN NEW;

ELSE RETURN OLD;



END IF;



END;

$$;


ALTER FUNCTION public.update_modified_column() OWNER TO postgres;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE accounts (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    user_id integer NOT NULL,
    "order" integer,
    currency_id integer NOT NULL,
    created_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    type integer DEFAULT 1 NOT NULL
);


ALTER TABLE accounts OWNER TO postgres;

--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE accounts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE accounts_id_seq OWNER TO postgres;

--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE accounts_id_seq OWNED BY accounts.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    user_id integer NOT NULL,
    data jsonb NOT NULL,
    created_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: currency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE currency (
    symbol character varying(10) NOT NULL,
    decimal_digits smallint NOT NULL,
    rounding smallint NOT NULL,
    code character varying(5) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE currency OWNER TO postgres;

--
-- Name: currency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE currency_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE currency_id_seq OWNER TO postgres;

--
-- Name: currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE currency_id_seq OWNED BY currency.id;


--
-- Name: entries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE entries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE entries_id_seq OWNER TO postgres;

--
-- Name: entries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE entries_id_seq OWNED BY entries.id;


--
-- Name: profiles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE profiles (
    user_id integer NOT NULL,
    base_currency_id integer NOT NULL
);


ALTER TABLE profiles OWNER TO postgres;

--
-- Name: rates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE rates (
    code character varying(5) NOT NULL,
    rate numeric(15,6) NOT NULL,
    created_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    updated_at timestamp(6) with time zone DEFAULT now() NOT NULL
);


ALTER TABLE rates OWNER TO postgres;

--
-- Name: ui_settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ui_settings (
    user_id integer NOT NULL,
    ui_calculate_negative boolean NOT NULL
);


ALTER TABLE ui_settings OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    email character varying(254) NOT NULL,
    password bytea NOT NULL,
    created_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    updated_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    id integer NOT NULL
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: accounts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accounts ALTER COLUMN id SET DEFAULT nextval('accounts_id_seq'::regclass);


--
-- Name: currency id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY currency ALTER COLUMN id SET DEFAULT nextval('currency_id_seq'::regclass);


--
-- Name: entries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entries ALTER COLUMN id SET DEFAULT nextval('entries_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: account_balances; Type: MATERIALIZED VIEW; Schema: public; Owner: postgres
--

CREATE MATERIALIZED VIEW account_balances AS
 SELECT accounts.id,
    COALESCE(sum(ledgers_on_date.amount), 0.0) AS balance
   FROM (accounts
     LEFT JOIN ledgers_on_date(now()) ledgers_on_date(account_id, entry_id, amount, entry_created, entry_updated) ON ((accounts.id = ledgers_on_date.account_id)))
  GROUP BY accounts.id
  ORDER BY COALESCE(accounts."order", 0)
  WITH NO DATA;


ALTER TABLE account_balances OWNER TO postgres;

--
-- Name: balances_on_date(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION balances_on_date(created timestamp with time zone) RETURNS SETOF account_balances
    LANGUAGE sql
    AS $$

  SELECT accounts.id,

    COALESCE(sum(ledgers_on_date.amount), 0.0) AS balance

   FROM (accounts

     LEFT JOIN ledgers_on_date(created) ledgers_on_date(account_id, entry_id, amount, entry_created, entry_updated) ON ((accounts.id = ledgers_on_date.account_id)))

  GROUP BY accounts.id

  ORDER BY COALESCE(accounts."order", 0)

$$;


ALTER FUNCTION public.balances_on_date(created timestamp with time zone) OWNER TO postgres;

--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (user_id);


--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: users email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT email UNIQUE (email);


--
-- Name: entries entries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entries
    ADD CONSTRAINT entries_pkey PRIMARY KEY (id);


--
-- Name: profiles profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (user_id);


--
-- Name: rates rates_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rates
    ADD CONSTRAINT rates_pkey PRIMARY KEY (code);


--
-- Name: ui_settings ui_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ui_settings
    ADD CONSTRAINT ui_settings_pkey PRIMARY KEY (user_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX code ON currency USING btree (code);


--
-- Name: accounts update_balances; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_balances AFTER INSERT OR DELETE OR UPDATE OF id OR TRUNCATE ON accounts FOR EACH STATEMENT EXECUTE PROCEDURE update_balances();


--
-- Name: entries update_balances; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_balances AFTER INSERT OR DELETE OR UPDATE OF amount, credit_id, debit_id, currency_ratio OR TRUNCATE ON entries FOR EACH STATEMENT EXECUTE PROCEDURE update_balances();


--
-- Name: rates update_timestamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_timestamp BEFORE UPDATE ON rates FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


--
-- Name: users update_timestamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_timestamp BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


--
-- Name: entries update_timestamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_timestamp BEFORE UPDATE ON entries FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


--
-- Name: categories update_timestamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_timestamp BEFORE UPDATE ON categories FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


--
-- Name: accounts update_timestamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_timestamp BEFORE UPDATE ON accounts FOR EACH ROW EXECUTE PROCEDURE update_modified_column();


--
-- Name: profiles base_currency_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT base_currency_id FOREIGN KEY (base_currency_id) REFERENCES currency(id) ON DELETE RESTRICT;


--
-- Name: rates code; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rates
    ADD CONSTRAINT code FOREIGN KEY (code) REFERENCES currency(code) ON DELETE RESTRICT;


--
-- Name: entries credit; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entries
    ADD CONSTRAINT credit FOREIGN KEY (credit_id) REFERENCES accounts(id) ON DELETE RESTRICT;


--
-- Name: accounts currency; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT currency FOREIGN KEY (currency_id) REFERENCES currency(id) ON DELETE RESTRICT;


--
-- Name: entries debit; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entries
    ADD CONSTRAINT debit FOREIGN KEY (debit_id) REFERENCES accounts(id) ON DELETE RESTRICT;


--
-- Name: accounts user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT;


--
-- Name: categories user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT;


--
-- Name: entries user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entries
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT;


--
-- Name: profiles user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT;


--
-- Name: ui_settings user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ui_settings
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT;


--
-- Name: postgraphql_watch_ddl; Type: EVENT TRIGGER; Schema: -; Owner: postgres
--

CREATE EVENT TRIGGER postgraphql_watch_ddl ON ddl_command_end
         WHEN TAG IN ('ALTER DOMAIN', 'ALTER FOREIGN TABLE', 'ALTER FUNCTION', 'ALTER SCHEMA', 'ALTER TABLE', 'ALTER TYPE', 'ALTER VIEW', 'COMMENT', 'CREATE DOMAIN', 'CREATE FOREIGN TABLE', 'CREATE FUNCTION', 'CREATE SCHEMA', 'CREATE TABLE', 'CREATE TABLE AS', 'CREATE VIEW', 'DROP DOMAIN', 'DROP FOREIGN TABLE', 'DROP FUNCTION', 'DROP SCHEMA', 'DROP TABLE', 'DROP VIEW', 'GRANT', 'REVOKE', 'SELECT INTO')
   EXECUTE PROCEDURE postgraphql_watch.notify_watchers_ddl();


ALTER EVENT TRIGGER postgraphql_watch_ddl OWNER TO postgres;

--
-- Name: postgraphql_watch_drop; Type: EVENT TRIGGER; Schema: -; Owner: postgres
--

CREATE EVENT TRIGGER postgraphql_watch_drop ON sql_drop
   EXECUTE PROCEDURE postgraphql_watch.notify_watchers_drop();


ALTER EVENT TRIGGER postgraphql_watch_drop OWNER TO postgres;

--
-- PostgreSQL database dump complete
--

