const logger = require('debug');

module.exports = (wildcard = '') => {
  const namespace = wildcard ? `${wildcard}:` : '';

  return {
    default: logger(`${namespace}info`),
    info: logger(`${namespace}info`),
    error: logger(`${namespace}error`),
    request: logger(`${namespace}request`),
    task: logger(`${namespace}task`),
    db: logger(`${namespace}db`),
  };
};
