FROM node:alpine

RUN apk update && apk upgrade && apk add rsync

RUN mkdir -p /tmp/app && \
    /usr/sbin/addgroup -g 980 app && \
    /usr/sbin/adduser -D -H -h /tmp/app -u 980 -G app app && \
    /bin/chown -R app:app /tmp/app

COPY . /tmp/app

WORKDIR /tmp/app

RUN yarn install
RUN yarn run bootstrap

CMD rsync -a -m --del /tmp/app/ /app/
