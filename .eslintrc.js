module.exports = {
  root: true,
  extends: ['airbnb', 'plugin:vue/recommended'],
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 8,
    sourceType: 'module',
  },
  rules: {
    'import/no-extraneous-dependencies': 0,
    'no-param-reassign': ['error', { ignorePropertyModificationsFor: ['app'] }],
    'import/no-unresolved': [
      'error',
      {
        ignore: ['~'],
      },
    ],
    'import/extensions': ['error', 'never', { packages: 'always' }],
    'no-shadow': 0,
    camelcase: 0,
  },
  overrides: [
    {
      files: ['*.vue'],
      plugins: ['vue'],
    },
    {
      files: ['*.js'],
      rules: {
        'vue/attributes-order': 0,
      },
    },
  ],
};
